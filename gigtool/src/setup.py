# -*- coding: utf-8 -*-

# A simple setup script to create an executable using Tkinter. This also
# demonstrates the method for creating a Windows executable that does not have
# an associated console.
#
# SimpleTkApp.py is a very simple type of Tkinter application
#
# Run the build process by running the command 'python setup.py build'
#
# If everything works well you should find a subdirectory in the build
# subdirectory that contains the files needed to run the application

import sys
import os
import opcode
import distutils
from cx_Freeze import setup, Executable

# import scipy
import pymzml
includefiles_list = []
# scipy_path = os.path.dirname(scipy.__file__)
pymzml_path = os.path.dirname(pymzml.__file__)
src_dir = os.path.dirname(os.path.abspath(__file__))
includefiles_list.append(pymzml_path + os.sep + "obo")
includefiles_list.append(src_dir+os.sep + "oxo.txt")

base = None
# if sys.platform == 'win32':
#     base = 'Win32GUI'

executables = [
    Executable('gigtool.py', base=base)
]

build_exe_options = {"packages": ["pymzml", "pandas",'numpy',"ttk","Tix"],"include_files": includefiles_list}

# distutils_path = os.path.join(os.path.dirname(opcode.__file__), 'distutils')
# print(distutils_path)
# build_exe_options = {'include_files': [(distutils_path, 'distutils')], "excludes": ["distutils"]}



setup(name='GIGTool',
      version='0.1',
      description='GIGTool, Search for Glycans',
      options={'build_exe': build_exe_options},
      executables=executables
      )
