import sys
import re
import getopt
from os import path


def main(argv):
    inputfile = 'test.csv'
    #inputfile = wkdir + "\\filename.csv"
    libfile = 'C:\\folder\\Olib.csv'
    # outfile = 'C:\\folder\\filename.csv'
    usage = 'OSearch.py -i <inputfile> '
    try:
        opts,args = getopt.getopt(argv,'hi:')
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    for opt,arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt == '-i':
            inputfile = arg
    basename,ext = path.splitext(path.basename(inputfile))
    wkdir = path.dirname(path.abspath(inputfile))
    outfile = wkdir + '\\' + basename + '_searchresult.csv'
    flib = open(libfile,'r')
    title = []
    underline = '_'
    comma = ','
    library = {}
    originmass = {}
    int_mass_map = {}
    for line in flib:
        l = line.rstrip()
        items = re.split(',',l)
        if not title:
            title = items
        else:
            comp = underline.join(items[:5])
            library[comp] = float(items[9])
            originmass[comp] = float(items[10])
            intmass = int(library[comp])
            if intmass not in int_mass_map:
                int_mass_map[intmass] = []
            int_mass_map[intmass].append(comp)

    flib.close()
    fout = open(outfile,'w')
    fres = open(inputfile,'r')
    for line in fres:
        l = line.rstrip()
        if re.match('#',l):
            fout.write(l+'\n')
        else:
            items = re.split(',',l)
            mh = float(items[4])
            intmh = int(mh)
            matches = []
            if intmh in int_mass_map:
                matches = int_mass_map[intmh]
            outstr = []
            for m in matches:
                # print(m)
                # print(library[m])
                outstr.append('{0:s}({1:f}/{2:f})'.format(m,library[m],originmass[m]))
            fout.write(l+',' + comma.join(outstr)+'\n')
    fres.close()
    fout.close()

if __name__ == "__main__":
    main(sys.argv[1:])