from Tkinter import *
import ttk
import Tix
import tkFileDialog
import tkMessageBox
import os, re, sys
import pandas as pd
import NComp
import OComp
import NGlycan
from NGlycan import haspeak
import NSearch
import OGlycan
import pymzml


class MyFirstGUI(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)

        self.master = master
        self.master.minsize(width=500, height=360)
        self.file_opt = options = {}
        master.title("GIG Tool")
        self.nlib = None
        self.olib = None
        self.nlib_mass_index = {}
        self.olib_mass_index = {}
        self.lib = None
        self.lib_mass_index = None
        self.mass_keys = {
            "N-linked": ["Mass[Na+]", "Mass[PT]", "Mass[PT_QU]"],
            "O-linked": ["Mass[Na+]",
                         "Mass[PMP_NoPT(H+)]",
                         "Mass[PMP_NoPT(Na+)]",
                         "Mass[PT(H+)]",
                         "Mass[PT_2PMP(H+)]",
                         "Mass"]
        }
        self.mass_key = StringVar(master, value="")

        self.data_label = Label(master, text="Input")
        self.data_label.grid(row=0, column=0, columnspan=3)
        self.data_folder = StringVar(master, value="")
        self.data_entry = Entry(master, textvariable=self.data_folder)
        self.data_entry.grid(row=0, column=3, columnspan=20, sticky=W + E)
        self.data_button = Button(master, text="Select", command=self.askopenfilenames)
        self.data_button.grid(row=0, column=23, sticky=W + E)


        self.data_listbox = Listbox(master)
        self.data_listbox.grid(row=1, column=0, columnspan=25, rowspan=4, sticky=W + E)
        self.filenames = []
        self.data_scroll = Scrollbar(master, orient=HORIZONTAL)
        self.data_scroll.grid(row=5, column=0, columnspan=25, sticky=W + E)
        self.data_listbox.config(xscrollcommand=self.data_scroll.set)
        self.data_scroll.config(command=self.data_listbox.xview)
        self.data_listbox.bind("<Delete>", self.key)


        self.out_folder = StringVar(master, value="")
        self.out_label = Label(master, text="Output:")
        self.out_label.grid(row=6, column=0, columnspan=3)
        self.out_entry = Entry(master, textvariable=self.out_folder)
        self.out_entry.grid(row=6, column=3, columnspan=20, sticky=W + E)

        self.sep = ttk.Separator(master, orient=HORIZONTAL)
        self.sep.grid(row=8, column=0, columnspan=30, sticky=W + E)
        self.option_label = Label(master, text="Options:").grid(row=8, column=0, columnspan=3)
        self.upper_row = 9
        self.upper_N_label = Label(master, text="N").grid(row=self.upper_row, column=12)
        self.N = StringVar(master, value=10)
        self.upper_N_entry = Entry(master, textvariable=self.N, width=5)
        self.upper_N_entry.grid(row=self.upper_row, column=13)
        self.upper_H_label = Label(master, text="H").grid(row=self.upper_row, column=14)
        self.H = StringVar(master, value=10)
        self.upper_H_entry = Entry(master, width=5, textvariable=self.H)
        self.upper_H_entry.grid(row=self.upper_row, column=15)
        self.upper_F_label = Label(master, text="F").grid(row=self.upper_row, column=16)
        self.F = StringVar(master, value=5)
        self.upper_F_entry = Entry(master, width=5, textvariable=self.F)
        self.upper_F_entry.grid(row=self.upper_row, column=17)
        self.upper_S_label = Label(master, text="S").grid(row=self.upper_row, column=18)
        self.S = StringVar(master, value=5)
        self.upper_S_entry = Entry(master, width=5, textvariable=self.S)
        self.upper_S_entry.grid(row=self.upper_row, column=19)
        self.upper_G_label = Label(master, text="G").grid(row=self.upper_row, column=20)
        self.G = StringVar(master, value=0)
        self.upper_G_entry = Entry(master, width=5, textvariable=self.G)
        self.upper_G_entry.grid(row=self.upper_row, column=21)

        self.glycan_type_label = Label(master, text="Glycan:")
        self.glycan_type_label.grid(row=9, column=0, columnspan=3, sticky=W)
        self.nlinked_checked = IntVar(master, value=1)
        self.nlinked_checkbtn = Checkbutton(master, text="N-linked", variable=self.nlinked_checked,
                                            command=self.nlinkchecked)
        self.nlinked_checkbtn.grid(row=9, column=3, columnspan=3)
        self.olinked_checked = IntVar(master, value=0)
        self.olinked_checkbtn = Checkbutton(master, text="O-linked", variable=self.olinked_checked,
                                            command=self.olinkchecked)
        self.olinked_checkbtn.grid(row=9, column=6, columnspan=3)

        # self.essoxo_label = Label(master,text="Essential").grid(row=9,column=0,columnspan=4)
        # self.nonessoxo_label = Label(master,text="Non-Essential").grid(row=9,column=5,columnspan=4)
        #
        # self.essoxo_list = Listbox(master).grid(row=10,column=0,columnspan=5,sticky=W+E)
        # self.nonessoxo_list = Listbox(master).grid(row=10,column=5,columnspan=5,sticky=W+E)
        self.mass_key_label = Label(master, text="Mass Type")
        self.mass_key_label.grid(row=11, column=0, columnspan=4, sticky=W)
        self.mass_key_combobox = ttk.Combobox(master, textvariable=self.mass_key)
        self.mass_key_combobox.grid(row=11, column=4, columnspan=5, sticky=W + E)
        self.mass_key_combobox['values'] = self.mass_keys["N-linked"]
        self.pmass_tol_label = Label(master, text="Precursor Mass Tolerance")
        self.pmass_tol_label.grid(row=11, column=11, columnspan=8, sticky=W)
        self.pmass_tol_value = IntVar(master, value=100)
        self.pmass_tol_entry = Entry(master, textvariable=self.pmass_tol_value, justify=RIGHT, width=4)
        self.pmass_tol_entry.grid(row=11, column=19, columnspan=2, sticky=W + E)
        self.pmass_tol_unit_label = Label(master, text="PPM")
        self.pmass_tol_unit_label.grid(row=11, column=21, columnspan=2, sticky=W)
        self.run_btn = Button(master, text="Run", command=self.run)
        self.run_btn.grid(row=14, column=0, columnspan=30, sticky=W + E)
        self.progress_value = IntVar(master, value=0)
        self.progress = ttk.Progressbar(master, mode='determinate', variable=self.progress_value, maximum=100)
        self.progress.grid(row=15, column=0, columnspan=30, sticky=W + S + E)

        # oxonium ions
        # src_dir = os.path.dirname(os.path.abspath(__file__))
        src_dir = None
        if getattr(sys, 'frozen', False):
            # frozen
            src_dir = os.path.dirname(sys.executable)
        else:
            # unfrozen
            src_dir = os.path.dirname(os.path.realpath(__file__))
        if not os.path.isfile(src_dir + "\\oxo.txt"):
            print("Error! Fail to find oxo.txt!")
            sys.exit()
        self.oxo_file = src_dir + "\\oxo.txt"
        self.df_oxo = pd.read_csv(self.oxo_file, sep=",")
        self.oxo = {}
        for index, row in self.df_oxo.iterrows():
            mz = row["m/z"]
            mz_int = int(row["m/z"])
            glycan_type = row["Glycan"]
            ess = row["Essential"]

            if ess == "yes":
                if "Essential" not in self.oxo:
                    self.oxo["Essential"] = {}
                if glycan_type not in self.oxo["Essential"]:
                    self.oxo["Essential"][glycan_type] = {}
                self.oxo["Essential"][glycan_type][mz_int] = [mz, 1]
            if ess == "no":
                if "Nonessential" not in self.oxo:
                    self.oxo["Nonessential"] = {}
                if glycan_type not in self.oxo["Nonessential"]:
                    self.oxo["Nonessential"][glycan_type] = {}
                self.oxo["Nonessential"][glycan_type][mz_int] = [mz, 1]

        self.oxo_label = Label(master, text="Oxonium Ions")
        self.oxo_label.grid(row=12, column=0, columnspan=4, sticky=W)
        self.oxo_button = Button(master, text="Settings",
                                 command=self.oxo_settings).grid(row=12, column=4, columnspan=5, sticky=W + E)

    def key(self,event):
        # print "pressed", repr(event.char)
        self.data_listbox.delete(ANCHOR)
        self.filenames = [str(i) for i in self.data_listbox.get(0,END)]


    def nlinkchecked(self):
        if self.nlinked_checked.get() == 1:
            self.olinked_checked.set(0)
            self.mass_key_combobox['values'] = self.mass_keys["N-linked"]
            self.mass_key_combobox.set("")
        else:
            self.olinked_checked.set(1)
            self.mass_key_combobox.set("")
            self.mass_key_combobox['values'] = self.mass_keys["O-linked"]

    def olinkchecked(self):
        if self.olinked_checked.get() == 1:
            self.nlinked_checked.set(0)
            self.mass_key_combobox.set("")
            self.mass_key_combobox['values'] = self.mass_keys["O-linked"]
        else:
            self.nlinked_checked.set(1)
            self.mass_key_combobox.set("")
            self.mass_key_combobox['values'] = self.mass_keys["N-linked"]

    def askopenfilenames(self):
        """Returns an opened file in read mode.
            This time the dialog just returns a filename and the file is opened by your own code.
        """
        # get filename
        filenames = tkFileDialog.askopenfilenames(**self.file_opt)

        # open file on your own
        if filenames:
            # return open(filenames, 'r')
            x = os.path.split(filenames[0])
            self.data_folder.set(x[0])
            self.out_folder.set(x[0] + "\\out")
            for filename in filenames:
                self.data_listbox.insert(END, filename)
                self.filenames.append(filename)

    def oxo_settings(self):
        self.tree = Tix.Tk()
        self.cl = Tix.CheckList(self.tree, browsecmd=self.update_oxo_list)
        self.cl.pack()
        for ess in self.oxo:
            self.cl.hlist.add(ess, text=ess)
            self.cl.setstatus(ess, "on")
            for gtype in self.oxo[ess]:
                self.cl.hlist.add("{0}.{1}".format(ess, gtype), text=gtype + "-linked")
                self.cl.setstatus("{0}.{1}".format(ess, gtype), "on")
                mz_list = sorted([int(i) for i in self.oxo[ess][gtype].keys()])
                for mz in mz_list:
                    self.cl.hlist.add("{0}.{1}.{2}".format(ess, gtype, int(mz)), text="{0}".format(int(mz)))
                    self.cl.setstatus("{0}.{1}.{2}".format(ess, gtype, int(mz)), "on")
        self.cl.autosetmode()

    def update_oxo_list(self, item):
        x = re.split("\.", item)
        label = 0
        status = "off"
        if self.cl.getstatus(item) == "on":
            print("turn on {0}".format(item))
            label = 1
            status = "on"
        elif self.cl.getstatus(item) == "off":
            print("turn off {0}".format(item))
            label = 0
            status = "off"
        if len(x) == 1:
            for i in self.oxo[x[0]]:
                self.cl.setstatus("{0}.{1}".format(x[0], i), status)
                for j in self.oxo[x[0]][i]:
                    self.oxo[x[0]][i][j][1] = label
                    self.cl.setstatus("{0}.{1}.{2}".format(x[0], i, j), status)
        elif len(x) == 2:
            for i in self.oxo[x[0]][x[1]]:
                self.oxo[x[0]][x[1]][i][1] = label
                self.cl.setstatus("{0}.{1}.{2}".format(x[0], x[1], i), status)
        elif len(x) == 3:
            self.oxo[x[0]][x[1]][int(x[2])][1] = label

            # for i in self.oxo:
            #     print(self.oxo[i])

    def run(self):
        global root
        self.run_btn.config(state="disabled")
        if not os.path.isdir(self.out_folder.get()):
            if self.out_folder.get() != "":
                try:
                    os.mkdir(self.out_folder.get())
                except:
                    print("ERROR! Fail to create output folder: {0}\n".format(self.out_folder.get()))
                    tkMessageBox.showinfo("ERROR!",
                                          "Fail to create output folder: {0}\n".format(self.out_folder.get()))
                    self.run_btn.config(state="normal")
                    return
        if len(self.filenames) == 0:
            tkMessageBox.showinfo("ERROR!", "Please select MS files")
            self.run_btn.config(state="normal")
            return
        # build theoretical glycan library

        if self.nlinked_checked.get():
            self.nlib = NComp.main(N=self.N.get(), H=self.H.get(),
                                   F=self.F.get(), S=self.S.get(),
                                   G=self.G.get(),
                                   wd=self.out_folder.get())
            cur_mass_key = str(self.mass_key.get())
            # print(cur_mass_key)
            for comp in self.nlib:
                if cur_mass_key not in self.nlib[comp]:
                    tkMessageBox.showinfo("ERROR!",
                                          "The mass of {0} is not in the database of N-linked glycan!".format(
                                              self.mass_key.get()))
                    self.run_btn.config(state="normal")
                    return
                mass_int = int(self.nlib[comp][self.mass_key.get()])
                if mass_int not in self.nlib_mass_index:
                    self.nlib_mass_index[mass_int] = []
                self.nlib_mass_index[mass_int].append(comp)

            self.lib = self.nlib
            self.lib_mass_index = self.nlib_mass_index

        if self.olinked_checked.get():
            self.olib = OComp.main(N=self.N.get(), H=self.H.get(),
                                   F=self.F.get(), S=self.S.get(),
                                   G=self.G.get(),
                                   wd=self.out_folder.get())
            for comp in self.olib:
                if self.mass_key not in self.olib[comp]:
                    tkMessageBox.showinfo("ERROR!",
                                          "The mass of {0} is not in the database of O-linked glycan!".format(
                                              self.mass_key.get()))
                    self.run_btn.config(state="normal")
                    return
                mass_int = int(self.olib[comp][self.mass_key])
                if mass_int not in self.olib_mass_index:
                    self.olib_mass_index[mass_int] = []
                self.olib_mass_index[mass_int].append(comp)
            self.lib = self.olib
            self.lib_mass_index = self.olib_mass_index
        # search each mzml file
        masstolerance = 100
        essen = {}
        nonessen = {}
        for mz_int in self.oxo["Essential"]["N"]:
            mz, status = self.oxo["Essential"]["N"][mz_int]
            if status == 1:
                essen[mz_int] = 1
        for mz_int in self.oxo["Nonessential"]["N"]:
            mz, status = self.oxo["Nonessential"]["N"][mz_int]
            if status == 1:
                nonessen[mz_int] = 1

        for mzml_file in self.filenames:
            self.progress_value.set(0)
            print("Start searching {0}. Please Wait...".format(mzml_file))
            basename, ext = os.path.splitext(os.path.basename(mzml_file))
            outputfile = self.out_folder.get() + "\\" + basename + ".csv"
            fout = open(outputfile, 'w')
            fout.write('#scan,charge,pmz,mass,MH+,coverage,')
            for i in sorted(essen.keys()):
                fout.write('{0:f},'.format(i))
            for i in sorted(nonessen.keys()):
                fout.write('{0:f},'.format(i))
            fout.write('candidate count,matches count')
            fout.write('\n')
            msrun = pymzml.run.Reader(mzml_file)
            # print(msrun.info["spectrum_count"])
            total_spectra = msrun.info["spectrum_count"]
            done_spectra = 0
            for spectrum in msrun:
                if spectrum['ms level'] == 2:
                    scanno = spectrum['id']
                    # print(scanno)
                    tmpessen = {}
                    tmpnonessen = {}
                    mzlist = spectrum.mz  # spectrum['MS:1000514']
                    ilist = spectrum.i  # spectrum['MS:1000515']

                    if self.nlinked_checked.get() == 1 or self.olinked_checked.get() == 1:
                        for mz_int in self.oxo["Essential"]["N"]:
                            mz, status = self.oxo["Essential"]["N"][mz_int]
                            if status == 1:
                                peak_to_find = haspeak(mz, masstolerance, mzlist, ilist)
                                if peak_to_find:
                                    (m, i) = peak_to_find
                                    tmpessen[mz] = i
                        for mz_int in self.oxo["Nonessential"]["N"]:
                            mz, status = self.oxo["Nonessential"]["N"][mz_int]
                            if status == 1:
                                peak_to_find = haspeak(mz, masstolerance, mzlist, ilist)
                                if peak_to_find:
                                    (m, i) = peak_to_find
                                    tmpnonessen[mz] = i
                                else:
                                    tmpnonessen[mz] = 0

                        if len(tmpessen) == len(essen.keys()):
                            charge = spectrum['precursors'][0]['charge']
                            pmz = spectrum['precursors'][0]['mz']
                            if (not charge) or (not pmz):
                                continue
                            pmass = (pmz * charge) - charge * 1.007825035
                            pmh = pmass + 1.007825035

                            tic = 0
                            for i in ilist:
                                tic += i
                            cover = 0
                            for mz in tmpessen:
                                cover += tmpessen[mz]
                            coverage = cover / tic
                            fout.write('{0:d},{1:d},{2:f},{3:f},{4:f},{5:.2f},'.format(scanno, charge, pmz, pmass, pmh,
                                                                                       coverage, ))
                            for i in sorted(tmpessen.keys()):
                                fout.write('{0:f},'.format(tmpessen[i]))
                            for i in sorted(tmpnonessen.keys()):
                                fout.write('{0:f},'.format(tmpnonessen[i]))

                            comp_candidates = []
                            for mz_int in range(int(pmh) - 1, int(pmh) + 2):
                                if mz_int in self.lib_mass_index:
                                    comp_candidates += self.lib_mass_index[mz_int]
                            fout.write("{0},".format(len(comp_candidates)))
                            results = []
                            for comp in comp_candidates:
                                comp_mass = self.lib[comp][self.mass_key.get()]
                                if (abs(comp_mass - pmh) / pmh) < (float(self.pmass_tol_value.get()) / 1000000):
                                    results.append(comp)
                            fout.write("{0},".format(len(results)))
                            fout.write(",".join(results))
                            fout.write('\n')
                done_spectra += 1
                pv = int(float(done_spectra) / float(total_spectra) * 100)
                if pv % 20 == 0:
                    self.progress_value.set(pv)
                    root.update()
            fout.close()
            print("Searching is Done.\nResult has been written in {0}.".format(outputfile))
        tkMessageBox.showinfo("Good News", "Mission Accomplished!")
        self.run_btn.config(state="normal")


root = Tk()
my_gui = MyFirstGUI(root)

root.resizable(width=False, height=False)
root.update()
root.mainloop()
