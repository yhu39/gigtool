import sys
import re
import getopt
from os import path

import pymzml

def main(argv):
    # inputfile = wkdir + "folder\\filename.mzML"
    inputfile = 'test.mzML'
    usage = 'OGlycan.py -i <inputfile> -o <outputfile>'
    try:
        opts,args = getopt.getopt(argv,'hi:')
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    for opt,arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt == '-i':
            inputfile = arg
    basename,ext = path.splitext(path.basename(inputfile))
    wkdir = path.dirname(path.abspath(inputfile))
    outputfile = wkdir + '\\' + basename + '.csv'
    essenfile = wkdir + "\\OEssen.txt"
    nonessenfile = wkdir + "\\ONonessen.txt"
    # print(path.abspath(inputfile))
    # print(path.dirname(path.abspath(inputfile)))
    # print(outputfile)

    essen = {}
    nonessen = {}
    # nonessen[363.1543] = 0
    # nonessen[204.0857] = 0
    # nonessen[138.0544] - 0
    fessen = open(essenfile,'r')
    for line in fessen:
        mz = float(line.rstrip())
        essen[mz] = 0
    print('Read in {0:d} essened mz value'.format(len(essen.keys())))
    print('Process {0:s}'.format(inputfile))
    fessen.close()
    fnonessen = open(nonessenfile,'r')
    for line in fnonessen:
        mz = float(line.rstrip())
        nonessen[mz] = 0
    print('Read in {0:d} nonessened mz value'.format(len(nonessen.keys())))
    # print('Process {0:s}'.format(inputfile))
    fnonessen.close()

    print(essen)
    print(nonessen)

    mzmlfile = inputfile
    msrun = pymzml.run.Reader(mzmlfile)

    fout = open(outputfile,'w')
    fout.write('#scan,charge,pmz,mass,MH+,coverage,')
    for i in sorted(essen.keys()):
        fout.write('{0:f},'.format(i))
    for i in sorted(nonessen.keys()):
        fout.write('{0:f},'.format(i))
    fout.write('\n')
    n = 0
    for spectrum in msrun:
        if spectrum['ms level'] == 2:
            n += 1
            if n % 100 == 0:
                sys.stderr.write('{0:d}...'.format(n))
                if n % 1000 == 0:
                    sys.stderr.write('\n')
                sys.stderr.flush()
            scanno = spectrum['id']
            essen_count = 0
            tmpessen = {}
            tmpnonessen = {}
            mzlist = spectrum.mz #spectrum['MS:1000514']
            ilist = spectrum.i #spectrum['MS:1000515']
            # for mz,i in spectrum.peaks:
            #     mzlist.append(mz)
            #     ilist.append(i)

            for mz in essen:
                peak_to_find = haspeak(mz,10,mzlist,ilist)
                if peak_to_find:
                    (m,i) = peak_to_find
                    tmpessen[mz] = i
            for mz in nonessen:
                peak_to_find = haspeak(mz,10,mzlist,ilist)
                if peak_to_find:
                    (m,i) = peak_to_find
                    tmpnonessen[mz] = i
                else:
                    tmpnonessen[mz] = 0

            if len(tmpessen) == len(essen.keys()):
                charge = spectrum['precursors'][0]['charge']
                pmz = spectrum['precursors'][0]['mz']
                pmass = (pmz * charge) - charge * 1.007825035
                pmh = pmass + 1.007825035

                tic = 0
                for i in ilist:
                    tic += i
                cover = 0
                for mz in tmpessen:
                    cover += tmpessen[mz]
                coverage = cover / tic
                fout.write('{0:d},{1:d},{2:f},{3:f},{4:f},{5:.2f},'.format(scanno,charge,pmz,pmass,pmh,coverage,))
                for i in sorted(tmpessen.keys()):
                    fout.write('{0:f},'.format(tmpessen[i]))
                for i in sorted(tmpnonessen.keys()):
                    fout.write('{0:f},'.format(tmpnonessen[i]))
                fout.write('\n')
    fout.close()

def haspeak(mz,ppm,mzlist,ilist):
    tol = ppm2th(mz,ppm)
    found = []
    for i in range(0,len(mzlist)):
        tmz = float(mzlist[i])
        ti = float(ilist[i])
        if tmz > mz + tol:
            break
        elif (tmz >= mz - tol) and (tmz <= mz + tol):
            if found:
                (oldmz,oldintensity) = found[0]
                if oldintensity < tmz:
                    found[0] = (tmz,ti)
            else:
                found.append((tmz,ti))
    if len(found) > 0:
        return found[0]
    else:
        return found


def ppm2th(mz,ppm):
    th = mz * ppm / 1000000
    return th

if __name__ == "__main__":
    main(sys.argv[1:])