import sys
import re
import os
import pandas as pd
# mass = {}
# mass['F'] = 146.058
# mass['N'] = 203.095
# mass['H'] = 162.0528
# mass['S'] = 291.0954
# mass['G'] = 307.0903
# mass['Na'] = 22.988
# mass['H2O'] = 18.0118
# mass['PMP'] = 174.079315
# mass['PT'] = 107.073502
# mass['+'] = 1.0079
mass = {}
mass['F'] = 146.057909
mass['N'] = 203.079373
mass['H'] = 162.052824
mass['S'] = 291.095417
mass['G'] = 307.090331
mass['Na'] = 22.989218
mass['H2O'] = 18.010565
mass['PMP'] = 174.079315
mass['PT'] = 107.073502
mass['+'] = 1.007276

# col F
def calc_mass_na(f, n, h, s, g):
    global mass
    m = f * mass['F'] + n * mass['N'] + h * mass['H'] + s * mass['S'] + g * mass['G']
    m += (1 + s + g) * mass['Na']
    m += mass['H2O']
    return m


# col I <- F
def calc_mass_pt_h(f, n, h, s, g):
    global mass
    m = calc_mass_na(f, n, h, s, g) + (s + g) * (mass['PT'] - mass['H2O'])
    m -= (s + g + 1) * mass['Na']
    m += mass['+']
    return m


# col J <- I
def calc_mass_pt_2pmp_h(f, n, h, s, g):
    global mass
    m = calc_mass_pt_h(f, n, h, s, g) + 2 * mass['PMP']
    m -= mass['H2O']
    return m


# col G <- J
def calc_mass_pmp_nopt_h(f, n, h, s, g):
    global mass
    m = calc_mass_pt_2pmp_h(f, n, h, s, g) - (s + g) * (mass['PT'] - mass['H2O'])
    return m


# col H  <- G
def calc_mass_pmp_nopt_na(f, n, h, s, g):
    global mass
    m = calc_mass_pmp_nopt_h(f, n, h, s, g) + mass['Na'] - mass['+']
    return m


# col M <- F
def calc_mass_origin(f, n, h, s, g):
    global mass
    m = calc_mass_na(f, n, h, s, g, ) - mass['Na'] * (1 + s + g)
    return m


def main(N,H,F,S,G,wd = ""):
    lib = {}
    lim = {
        'F': int(F),
        'N': int(N),
        'H': int(H),
        'S': int(S),
        'G': int(G)
    }
    out_file = "olib.csv"
    if wd:
        out_file = wd + "\\" + out_file
    if os.path.isfile(out_file):
        print("NOTE! The O-glycan database exists. Let's just use it and save time!")
        print("O-Glycan database: {0}".format(out_file))
        df = pd.read_csv(out_file)
        for index, row in df.iterrows():
            comp = row["name"]
            lib[comp] = {}
            for column in df.columns.values:
                lib[comp][column] = row[column]
        return
    fout = open(out_file, 'w')
    glycan_count = 0
    for f in range(0, lim['F'] + 1):
        for n in range(0, lim['N'] + 1):
            for h in range(0, lim['H'] + 1):
                for s in range(0, lim['S'] + 1):
                    for g in range(0, lim['G'] + 1):
                        if f + n + h + s + g == 0:
                            fout.write('F,N,H,S,G,')
                            fout.write('Mass[Na+],'+'Mass[PMP_NoPT(H+)],'+'Mass[PMP_NoPT(Na+)],'+'Mass[PT(H+)],'+'Mass[PT_2PMP(H+)],'+'Mass,name\n')
                            continue
                        mass_na = calc_mass_na(f, n, h, s, g)
                        mass_pmp_nopt_h = calc_mass_pmp_nopt_h(f, n, h, s, g)
                        mass_pmp_nopt_na = calc_mass_pmp_nopt_na(f, n, h, s, g)
                        mass_pt_h = calc_mass_pt_h(f, n, h, s, g)
                        mass_pt_2pmp_h = calc_mass_pt_2pmp_h(f, n, h, s, g)
                        mass_origin = calc_mass_origin(f, n, h, s, g)
                        outmass = '{0:f},{1:f},{2:f},{3:f},{4:f},{5:f}'.format(mass_na,
                                                                               mass_pmp_nopt_h,
                                                                               mass_pmp_nopt_na,
                                                                               mass_pt_h,
                                                                               mass_pt_2pmp_h,
                                                                               mass_origin)
                        comp = 'F{0:d}N{1:d}H{2:d}S{3:d}G{4:d}'.format(f, n, h, s, g)
                        output = '{0:d},{1:d},{2:d},{3:d},{4:d},{5:s},{6}'.format(f, n, h, s, g, outmass,comp)

                        fout.write('{0:s}\n'.format(output))
                        glycan_count += 1
    fout.close()
    print("The O-Glycan database is written to {0}, total Glycans: {1}".format(out_file, glycan_count))
    return lib

if __name__ == "__main__":
    main(10,10,10,10,10)
