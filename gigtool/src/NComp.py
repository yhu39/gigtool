import sys
import re
import os
import pandas as pd
# mass = {}
# mass['F'] = 146.058
# mass['N'] = 203.095
# mass['H'] = 162.0528
# mass['S'] = 291.0954
# mass['G'] = 307.0903
# mass['Na'] = 22.988
# mass['H2O'] = 18.0118
# mass['PMP'] = 174.079315
# mass['PT'] = 107.073502
# mass['+'] = 1.0079
mass = {}
mass['F'] = 146.057909
mass['N'] = 203.079373
mass['H'] = 162.052824
mass['S'] = 291.095417
mass['G'] = 307.090331
mass['Na'] = 22.989218
mass['H2O'] = 18.010565
mass['PMP'] = 174.079315
mass['PT'] = 107.073502
mass['+'] = 1.007276
mass['core'] = mass['H2O'] + 2 * mass['N'] + 3 * mass['H'] + mass['Na']
mass['qu'] = 267.2201
mass['O'] = 15.99492

def calc_m(f,n,h,s,g):
    global mass
    m = mass['F']*f + mass['N']*n + mass['H']*h + mass['S']*s + mass['G']*g +(mass['Na']-mass['+'])*(s+g) + mass['core']
    return m

def calc_pt(f,n,h,s,g):
    global mass
    m = calc_m(f,n,h,s,g) + (s+g) * (mass['PT']-mass['H2O'] - mass['Na']) + mass['+']
    return m

def calc_pt_qu(f,n,h,s,g):
    global mass
    m = calc_pt(f,n,h,s,g) + mass['qu'] - mass['O'] - mass['Na'] + mass['+'] - mass['H2O']
    return m




def main(N,H,F,S,G,wd=""):
    lib = {}
    lim = {'F': int(F), 'N': int(N), 'H': int(H), 'S': int(S), 'G': int(G)}
    out_file = "nlib.csv"
    if wd:
        out_file = wd + "\\" + out_file
    if os.path.isfile(out_file):
        print("NOTE! The N-Glycan database exists. Let's just use it and save time!")
        print("N-Glycan database: {0}".format(out_file))
        df = pd.read_csv(out_file)
        for index,row in df.iterrows():
            comp = row["name"]
            lib[comp] = {}
            for column in df.columns.values:
                lib[comp][column] = row[column]
        return lib
    fout = open(out_file, 'w')
    fout.write('F,N,H,S,G,Mass[Na+],Mass[PT],Mass[PT_QU],name\n')
    glycan_count = 0
    for f in range(0, lim['F'] + 1):
        for n in range(2, lim['N'] + 1):
            for h in range(3, lim['H'] + 1):
                for s in range(0, lim['S'] + 1):
                    for g in range(0, lim['G'] + 1):
                        mass_m = calc_m(f,n-2,h-3,s,g)
                        mass_pt = calc_pt(f,n-2,h-3,s,g)
                        mass_pt_qu = calc_pt_qu(f,n-2,h-3,s,g)
                        # comp = 'F{0:d}N{1:d}H{2:d}S{3:d}G{4:d}'.format(f,n+2,h+3,s,g)
                        comp = 'F{0:d}N{1:d}H{2:d}S{3:d}G{4:d}'.format(f, n, h, s, g)
                        outmass = '{0:f},{1:f},{2:f},{3:s}'.format(mass_m,mass_pt,mass_pt_qu,comp)
                        output = '{0:d},{1:d},{2:d},{3:d},{4:d},{5:s}'.format(f, n, h, s, g, outmass)
                        fout.write('{0:s}\n'.format(output))
                        glycan_count += 1
                        lib[comp] = {
                            "Mass[Na+]": mass_m,
                            "Mass[PT]": mass_pt,
                            "Mass[PT_QU]": mass_pt_qu
                        }
    fout.close()
    print("The N-Glycan database is written to {0}, total Glycans: {1}".format(out_file,glycan_count))
    return lib

if __name__ == "__main__":
    main(12,12,10,10,10)
