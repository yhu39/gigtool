import os
import sys
import glob
import subprocess
import tkMessageBox
from Tkinter import *

parent_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(parent_path)

pipeline_path = parent_path + '\\'
install_path = pipeline_path + 'installs\\'
programs_path = install_path + 'windows\\'

def main():
    root = Tk()
    root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
    root.update_idletasks()
    # tkMessageBox.showinfo('Alert!', 'Java, 7-Zip, ProteoWizard, and PuTTY will be installed.  You may skip the installations for software that is already installed on your computer.')
    root.destroy()
    
    # msi_files = glob.glob(programs_path + '*.msi')
    # exe_files = glob.glob(programs_path + '*.exe')
    
    # for msi in msi_files:
    #     subprocess.call('msiexec /i ' + msi)
    #
    # for exe in exe_files:
    #     subprocess.call(exe)
        
    # subprocess.call('python ' + programs_path + 'get-pip.py')
    #subprocess.call('pip install pycrypto')
    subprocess.call('pip install pymzml')
    subprocess.call('pip install pandas')
    #subprocess.call('pip install aws-shell')
    
    # p0a.main()
    
    print '\nDone!'
    
    root = Tk()
    root.geometry('%dx%d+%d+%d' % (0, 0, 0, 0))
    root.update_idletasks()
    tkMessageBox.showinfo('Success!', 'Installation was completed successfully!')
    root.destroy()

if __name__ == '__main__':
    main()    