GIG Tool is a software to search N-linked and O-lined glycans in LC-MS/MS experiment.
The precursor mass of each MS2 scan is searched against the known or theoretical database of glycans to obtain the glycan-spectrum matchings (GSMs).

Installation:
1. Download Python 2.7 from https://www.python.org/
2. Install Python 2.7 to a specified path, for example, E:\Python27
3. Add Python folder to the system path. 
In Windows, Right Click "My Computer" > "Properties" > "Advanced System Settings" > "Environment Variables" 
Add "E:\Python27;E:\Python27\Scripts;" to the variable "Path" in "system variables" or "User variables". 
4. Unzip source code package to a specified path, for example, E:\gigtool
5. Intall dependency packages of Python: "pandas" and "pymzml". 
In Windows, open the "E:\gigtool\install" folder and double click "program_installs.bat" or run it in a command line prompt.
6. Start GIGTool. 
In Windows, open the "E:\gigtool\src" folder and double click "gigtool.bat" or run it in a command line prompt.

